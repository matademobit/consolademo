﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demoObjetos
{
    class Curso
    {
        private string nombre;
        private string horasSemanales;
        private Profesor profesor;


        public void setnombre(string p_nombre)
        {
            this.nombre = p_nombre;
        }

        public string getnombre()
        {

            return this.nombre;
        }
        public void sethorasSemanales(string p_horasSemanales)
        {
            this.horasSemanales = p_horasSemanales;
        }

        public string gethorasSemanales()
        {

            return this.horasSemanales;
        }

        public void setProfesor(Profesor prof)
        {
            this.profesor = prof;
            prof.setCurso(this);

        }

        public Profesor getProfesor()
        {

            return this.profesor;
        }


    }
}
