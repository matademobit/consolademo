﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace demoObjetos
{
    class Program
    {
        static void Main(string[] args)
        {
            Alumno alu1 = new Alumno();
            alu1.setDni("24587416");
            alu1.setEdad("44");
            alu1.setNombre("Matias");

            Alumno alu2 = new Alumno();
            alu2.setDni("33444555");
            alu2.setEdad("34");
            alu2.setNombre("Raul");

            Alumno alu3 = new Alumno("22777999", "Jorge", "221");

            Profesor prof1 = new Profesor();
            prof1.setDni("2000000");
            prof1.setNombre("Juan Carlos Parra");
            prof1.setEspecialidad("Comunicaciones");

            Curso cur1 = new Curso();
            cur1.setnombre("Intro a la Comunicacion");
            cur1.sethorasSemanales("30");
            cur1.setProfesor(prof1);
       //     prof1.setCurso(cur1);

            Curso cur2 = new Curso();
            cur2.setnombre("Matematica");
            cur2.sethorasSemanales("16");
            cur2.setProfesor(prof1);
         //   prof1.setCurso(cur2);


            foreach(Curso curs in prof1.getCursos())
            {

                Console.WriteLine("el curso se llama: " + curs.getnombre());
                    
            }




            Console.ReadLine();

        }
    }
}
