﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demoObjetos
{
    class Profesor
    {
        private string dni;
        private string nombre;
        private string especialidad;
        private List<Curso> cursos;

        public Profesor()
        {
            this.cursos = new List<Curso>();
        }
        
        public void setDni(string dni)
        {
            this.dni = dni;
        }

        public string getDni()
        {

            return this.dni;
        }
        public void setNombre(string p_nombre)
        {
            this.nombre = p_nombre;
        }

        public string getNombre()
        {

            return this.nombre;
        }

        public void setEspecialidad(string p_espe)
        {
            this.especialidad = p_espe;
        }

        public string getEspecialidad()
        {

            return this.especialidad;
        }

        public void setCurso(Curso cur)
        {
            this.cursos.Add(cur);
        }
        
        public List<Curso> getCursos()
        {

            return this.cursos;
        }


    }
}
